﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorMarcosDeBalas : MonoBehaviour
{
    public Sprite marcoStandar;
    public Sprite MarcoExplosivo;
    public Sprite marcoRayo;

    public WeaponManager weapon_Manager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
            if (weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.DISPARONOMRAL)
            {
                GetComponent<Image>().sprite = marcoStandar;

            }

            if (weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.EXPLOSIVO)
            {
                GetComponent<Image>().sprite = MarcoExplosivo;
            }

            if (weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.RAYO)
            {
            GetComponent<Image>().sprite = marcoRayo;
            }

    }
}
