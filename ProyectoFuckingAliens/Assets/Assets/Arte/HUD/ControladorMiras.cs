﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorMiras : MonoBehaviour
{

    public Sprite miraStandar;
    public Sprite miraLanzaGranadas;
    public Sprite miraRayo;

    public WeaponManager weapon_Manager;

    private void Awake()
    {
       
    }
    
    void Start()
    {
        
    }

    
    void Update()
    {
        if(weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.DISPARONOMRAL)
        {
            GetComponent<Image>().sprite = miraStandar;

        }

        if (weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.EXPLOSIVO)
        {
            GetComponent<Image>().sprite = miraLanzaGranadas;
        }

        if (weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.RAYO)
        {
            GetComponent<Image>().sprite = miraRayo;
        }


    }
}
