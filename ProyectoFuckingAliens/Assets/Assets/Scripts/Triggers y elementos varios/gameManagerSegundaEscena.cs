﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManagerSegundaEscena : MonoBehaviour
{
    public GameObject jugador;
    public GameObject[] jugadores;
   
    void Start()
    {
        jugadores = GameObject.FindGameObjectsWithTag("player");

        foreach(GameObject jugador in jugadores)
        {
            jugador.transform.position = transform.position;
        }

        ControladorTextoMisiones.Texto("mision   activa:    mata   a   todo   lo   que  se   mueva");
    }

    
    void Update()
    {
        
    }
}
