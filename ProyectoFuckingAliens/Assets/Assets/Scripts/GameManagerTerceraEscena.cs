﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerTerceraEscena : MonoBehaviour
{
    public GameObject jugador;
    public GameObject[] jugadores;

    void Start()
    {
        jugadores = GameObject.FindGameObjectsWithTag("player");

        foreach (GameObject jugador in jugadores)
        {
            jugador.transform.position = transform.position;
        }

        ControladorTextoMisiones.Texto("mision   activa: Llega hasta el segundo poblado y mata a los enemigos");
    }
}
