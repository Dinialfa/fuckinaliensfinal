﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtaManager : MonoBehaviour
{
    public int maxVida;
    public int vida;
    public BarraDeVida barraDeVida;

    //public BarraDeVida barraDeVida;

    private void Awake()
    {
        vida = 100;
        maxVida = 100;
        barraDeVida.SetMaxHealth(maxVida);
    }
    void Start()
    {
        barraDeVida.SetMaxHealth(maxVida);
    }

    void Update()
    {
        if (vida <= 0)
        {
            //Debug.Log("TE HAN MATADO DANI");
            //Que pasen cosas como que te lleven a una pantalla de muerte o lo que sea no?
        }
    }

    public void AumentarVida(int vidaIncremento)
    {
        vida += vidaIncremento;
        barraDeVida.SetHealth(vida);
    }

    public void RecibirDaño(int daño)
    {
        vida -= daño;
        barraDeVida.SetHealth(vida);
    }
}
