﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equiper : MonoBehaviour
{

    public WeaponHandler arma;

    public float velocidadRotacion = 25f;

    private float tiempoRotacion = 0.7f;
    public int signo = 1;
    public float tiempoUsuario = 1f;

    Rigidbody rb;


    public void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    public void Update()
    {
        Timer(tiempoUsuario);

        Movimiento();
    }
    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "player")
        {
            //Debug.Log("He añadido un arma");
            if (other.GetComponentInChildren<WeaponManager>() != null)
            {
                if(other.GetComponentInChildren<WeaponManager>().weaponsPrueba.Contains(arma) == false)
                {
                    other.GetComponentInChildren<WeaponManager>().weaponsPrueba.Add(arma);

                    Destroy(gameObject);
                }
                
            }
                     
        }
    }

    public void Movimiento()
    {
        //Rotacion
        Quaternion rotacion = Quaternion.AngleAxis(velocidadRotacion, Vector3.up);

        transform.rotation = Quaternion.Euler(rotacion.x, rotacion.y, rotacion.z) * transform.rotation;

        transform.Translate(Vector3.up * signo * 0.02f);
        
    }

    public void Timer(float tiempoDespues)
    {
        tiempoRotacion -= Time.deltaTime;
        if (tiempoRotacion < 0)
        {
            signo = signo * -1;
            tiempoRotacion = tiempoDespues;
        }
    }
}
