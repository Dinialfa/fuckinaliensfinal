﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerMisiones : MonoBehaviour
{
    public GameObject jugador;
    public GameObject[] jugadores;

    KillGoal mision1 = new KillGoal(1, false, 0, 2);
    KillGoal mision2 = new KillGoal(2, false, 0, 4);
    KillGoal mision3 = new KillGoal(3, false, 0, 2);
    KillGoal mision4 = new KillGoal(4, false, 0, 2);
    KillGoal mision5 = new KillGoal(5, false, 0, 3);
    KillGoal mision6 = new KillGoal(6, false, 0, 6);


    public GameObject[] PuertasDesactivar = new GameObject[13];

    public bool Escena1;
    

    
    

    void Start()
    {
        ControladorTextoMisiones.Texto("mision   activa:    investiga   la   cueva ");
        
        mision1.Init();
        mision2.Init();
        mision3.Init();
        mision4.Init();
        mision5.Init();
        mision6.Init();


        jugadores = GameObject.FindGameObjectsWithTag("player");

        foreach (GameObject jugador in jugadores)
        {
            jugador.transform.position = transform.position;
        }
    }

    void Update()
    {
        
        if (mision1.completed == true)
        {
            
            Destruir2puertas(PuertasDesactivar[0], PuertasDesactivar[1]);
        }

        if (mision2.completed == true)
        {
            
            Destruir(PuertasDesactivar[2]);
        }

        if (mision3.completed == true)
        {
            
            Destruir(PuertasDesactivar[3]);
        }

        if (mision4.completed == true)
        {
            
            Destruir(PuertasDesactivar[4]);
        }

        if (mision5.completed == true)
        {
            
            Destruir(PuertasDesactivar[5]);
        }

        if(mision6.completed == true)
        {
            ControladorTextoMisiones.Texto("Mmision activa:    Metete  en  el  portal");
            AparecerTrigger(PuertasDesactivar[6]);
        }
    }

    public void Destruir2puertas(GameObject PuertasDesactivar, GameObject PuertasDesactivar2)
    {
        
        Destroy(PuertasDesactivar);
        Destroy(PuertasDesactivar2);

    }

    public void Destruir(GameObject PuertasDesactivar)
    {
        Destroy(PuertasDesactivar);
    }

    public void AparecerTrigger(GameObject PuertasActivar)
    {
        PuertasActivar.SetActive(true);
    }
}
