﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillGoal : Goal
{
    public int EnemyID { get; set; }
    
    public KillGoal(int enemyID, bool completed, int currentAmount, int requiredAmount)
    {
        this.EnemyID = enemyID;
        this.completed = completed;
        this.currentAmount = currentAmount;
        this.requiredAmount = requiredAmount;
    }

    public override void Init()
    {
        base.Init();
        CombatEvents.OnEnemyDeath += EnemyDied;
    }

    void EnemyDied(Torretas torreta)
    {
        if(torreta.ID == this.EnemyID)
        {
            this.currentAmount += 1;
            Evaluate();
        }
    }
}
