﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatEvents : MonoBehaviour
{
    public delegate void EnemyEventHandler(Torretas enemigoTorreta);

    public static event EnemyEventHandler OnEnemyDeath; 

    public static void EnemyDied(Torretas enemigoTorreta)
    {
        if(OnEnemyDeath != null)
        {
            OnEnemyDeath(enemigoTorreta);
        }
        
    }
}
