﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorCoches : MonoBehaviour
{
    
    public float tiempoEspera;
    public GameObject coche;
    
    void Start()
    {
        tiempoEspera = 4;
    }

    
    void Update()
    {
        tiempoEspera -= Time.deltaTime;

        if(tiempoEspera <= 0)
        {
            Instantiate(coche, transform.position, transform.rotation);
            
            tiempoEspera = Random.Range(3, 15);
        }
        
    }
}
