﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoCoche : MonoBehaviour
{
    public float velocidadCoche;

    public Transform lugarRayo;

    public float rangoRayo;

    public float tiempoDeVida;
    void Start()
    {
        velocidadCoche = 40;
        rangoRayo = 3;
        tiempoDeVida = 7.5f;
    }

    
    void Update()
    {
        transform.Translate(new Vector3(0,-1,0) * velocidadCoche * Time.deltaTime);

        RaycastHit ray;
        Physics.Raycast(lugarRayo.position, new Vector3(0, 0, 1), out ray, rangoRayo);

        tiempoDeVida -= Time.deltaTime;

        if(ray.transform != null)
        {
            if (ray.transform.GetComponent<ProtaManager>() != null)
            {
                
                ProtaManager protaManager = ray.transform.GetComponent<ProtaManager>();
                protaManager.RecibirDaño(40);
            }
        }

        if(tiempoDeVida <= 0)
        {
            Destroy(gameObject);
        }
       
    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(lugarRayo.position, new Vector3(0, 0, 1));
    }




}
