﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaEnemigoPatrullero : Enemigo
{
    // Start is called before the first frame update
    void Start()
    {
        vida = 20;
    }

    // Update is called once per frame
    void Update()
    {
        if(vida <= 0)
        {
            Destroy(gameObject);
        }
    }
}
