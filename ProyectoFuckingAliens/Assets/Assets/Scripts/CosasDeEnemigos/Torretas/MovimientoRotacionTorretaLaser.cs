﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//public enum TipoTorreta { STANDAR, LASER }
public class MovimientoRotacionTorretaLaser : Torretas
{

    //public TipoTorreta tipoTorreta;
    public float velocidadRotacion;
    
    public int signo = 1;
    //public int ID = 1;

    private float tiempoRotacion = 2f;
    public float tiempoRotacionUsuario;

    public Transform ComienzoRayo;

    public float rango = 20f;
    public int daño = 5;


    public float velocidadDañar = 0.2f;
    private float nexFire = 0.5f;
    private float myTime = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        rango = 20f;
        daño = 5;

        myTime = 0.0f;
        nexFire = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
       Timer(tiempoRotacionUsuario);
 
       Rotar(signo);

       Laser();

        
       myTime += Time.deltaTime; //Contador de segundos
    }

    public void Rotar(int signo)
    {    
        Quaternion rotacion = Quaternion.AngleAxis(velocidadRotacion, Vector3.up);
        
        transform.rotation = Quaternion.Euler(rotacion.x, rotacion.y * signo, rotacion.z) * transform.rotation;    
      
    }

    public void Timer(float tiempoDespues)
    {
        tiempoRotacion -= Time.deltaTime;
        if(tiempoRotacion < 0)
        {
            signo = signo * -1;
            tiempoRotacion = tiempoDespues;
        }
    }

    public void Laser()
    {
        RaycastHit hit;
        Physics.Raycast(ComienzoRayo.position, ComienzoRayo.forward, out hit, rango);

        if(hit.transform != null)
        {
            while (hit.transform.GetComponent<ProtaManager>() != null && myTime > nexFire)
            {
                nexFire = myTime + velocidadDañar;


                ProtaManager vidaProta = hit.transform.GetComponent<ProtaManager>();
                
                vidaProta.RecibirDaño(daño);

            }

            nexFire = nexFire - myTime;
            myTime = 0.0f;
        }
    }

    private void OnDrawGizmos()
    {
        
        Gizmos.DrawRay(ComienzoRayo.position, ComienzoRayo.forward);
    }


}
