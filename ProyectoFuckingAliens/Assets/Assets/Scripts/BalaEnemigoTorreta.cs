﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaEnemigoTorreta : MonoBehaviour
{
    public int dañoBalaEnemigo = 15;
    

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "player")
        {

            if (collision.gameObject.GetComponent<ProtaManager>() != null)
            {
                ProtaManager protaManager = collision.gameObject.GetComponent<ProtaManager>();

                protaManager.RecibirDaño(dañoBalaEnemigo);

                Destroy(gameObject);
            }
        }
    }
}
