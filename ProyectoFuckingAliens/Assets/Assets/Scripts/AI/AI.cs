﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateStuff;
using UnityEngine.AI;

public class AI : MonoBehaviour
{
    public StateMachine<AI> stateMachine { get; set; }

    public bool switchState = false;

    public Rigidbody rb;
    public Animator animator;
    
    

    public float RangoDeteccion;
    public Transform target;
    Vector3 targetDestinoRandom;

    
    public float timeForNewPath;
    public bool inCoroutine;
    public float timeforNewRotation;
    

    public int velocidadRotacion;
    public int signo;
    public bool moverse;
    public bool rotar;


    public float fireCountdown;
    public float fuerzaBalaTorreta;
    public GameObject balaTorreta;
    public float fireRate = 0.5f;
    public Transform lugarDisparo;
    public GameObject armaVisual;


    private void Start()
    {
        stateMachine = new StateMachine<AI>(this);
        stateMachine.ChangeState(FirstState.Instance);
        timeForNewPath = 1;

        velocidadRotacion = 40;

        fireRate = 2f;
        
        timeforNewRotation = 3;
        fuerzaBalaTorreta = 40;

        RangoDeteccion = 30;
       
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();

    }

    private void Update()
    {

        GameObject[] objetivos = GameObject.FindGameObjectsWithTag("player");
        GameObject nearestObjetivo = null;
        float nearestDistancia = Mathf.Infinity;


        foreach (GameObject enemigos in objetivos)
        {
            float distancia = Vector3.Distance(transform.position, enemigos.transform.position);

            if(distancia < nearestDistancia)
            {
                nearestDistancia = distancia;
                nearestObjetivo = enemigos;
            }
        }

        if(nearestObjetivo != null && nearestDistancia <= RangoDeteccion)
        {
            switchState = true;
            target = nearestObjetivo.transform;
        }
        else
        {
            switchState = false;
            target = null;
        }

        if(moverse == true)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * 2);
        }

        if(rotar == true)
        {
            Quaternion rotacion = Quaternion.AngleAxis(velocidadRotacion, Vector3.up);

            transform.rotation = Quaternion.Euler(rotacion.x, rotacion.y * signo, rotacion.z) * transform.rotation;
        }

        

        stateMachine.Update();
    }

    public void setRotacionHaciaJugador()
    {
        Vector3 dir = target.position - transform.position;

        //Rotacion con la informacion en quaterniones
        Quaternion lookRotation = Quaternion.LookRotation(dir);

        //Pasamos esa informacion de quaterniones a informacion de rotacion estandar de unity
        Vector3 rotacionReal = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * 10f).eulerAngles;

        transform.rotation = Quaternion.Euler( 0 , rotacionReal.y, 0);
    }
    

    public void DispararJugador()
    {
        GameObject bulletTorreta = Instantiate(balaTorreta, lugarDisparo.position, transform.rotation);
        Rigidbody rb = bulletTorreta.GetComponent<Rigidbody>();

        rb.AddForce(lugarDisparo.forward * fuerzaBalaTorreta, ForceMode.Impulse);
    }


    public IEnumerator DoSomething()
    {
        inCoroutine = true;
        moverse = true;
        rotar = false;
        yield return new WaitForSeconds(timeForNewPath);

        signo = Random.Range(-1, 2);
        rotar = true;
        moverse = false;

        yield return new WaitForSeconds(2);
        inCoroutine = false;
        moverse = false;
        rotar = false;
    }

    public void GetNewPath()
    {
        Debug.Log("Estoy rotando");
        Quaternion rotacion = Quaternion.AngleAxis(velocidadRotacion, Vector3.up);

        transform.rotation = Quaternion.Euler(rotacion.x, rotacion.y * Random.Range(-1,1), rotacion.z) * transform.rotation;

    }

    
}
