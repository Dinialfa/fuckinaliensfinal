﻿using StateStuff;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstState : State<AI>
{
    private static FirstState _instance;

    
    private FirstState()
    {
        if(_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static FirstState Instance
    {
        get
        {
            if(_instance == null)
            {
                new FirstState();
            }

            return _instance;
        }
    }
    public override void EnterState(AI _owner)
    {
        
    }

    public override void ExitState(AI _owner)
    {
        
    }

    public override void UpdateState(AI _owner)
    {
        

        if(_owner.target != null)
        {
            _owner.setRotacionHaciaJugador();
            _owner.fireCountdown -= Time.deltaTime;

            if (_owner.fireCountdown <= 0)
            {
                _owner.animator.SetBool("Andando", false);
                _owner.animator.SetBool("Disparar", true);
                _owner.armaVisual.SetActive(true);
                _owner.DispararJugador();
                _owner.fireCountdown = 1f / _owner.fireRate;
            }
        }

        if(_owner.switchState == false)
        {
            _owner.stateMachine.ChangeState(SecondState.Instance);
        }

    }
    
    
}
