﻿using StateStuff;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondState : State<AI>
{
    private static SecondState _instance;


    private SecondState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static SecondState Instance
    {
        get
        {
            if (_instance == null)
            {
                new SecondState();
            }

            return _instance;
        }
    }
    public override void EnterState(AI _owner)
    {
       
    }

    public override void ExitState(AI _owner)
    {
        
    }

    public override void UpdateState(AI _owner)
    {
        

        if(_owner.inCoroutine == false)
        {
            _owner.animator.SetBool("Andando", true);
            _owner.animator.SetBool("Disparar", false);
            _owner.armaVisual.SetActive(false);
            _owner.StartCoroutine(_owner.DoSomething());
            Debug.Log("Estoy en la corrutina");
            
        }

            

        if (_owner.switchState == true)
        {
            _owner.stateMachine.ChangeState(FirstState.Instance);
        }
    }


}

