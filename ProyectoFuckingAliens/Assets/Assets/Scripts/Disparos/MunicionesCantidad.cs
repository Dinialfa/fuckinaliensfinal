﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MunicionesCantidad : MonoBehaviour
{
    public int municionStandar = 50;
    public int municionExplosiva = 20;
    
    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        //Debug.Log("estoy tocando algo");

        if(other.CompareTag("municionStandar") == true)
        {
            //Debug.Log("gane municion");
            municionStandar += 20;
        }

        if (other.tag == "municionExplosiva")
        {
            municionExplosiva += 5;
        }
    }

    public int devolverMunicionStandar()
    {
        return municionStandar;
    }

    public int devolverMunicionExplosiva()
    {
        return municionExplosiva;
    }
}
