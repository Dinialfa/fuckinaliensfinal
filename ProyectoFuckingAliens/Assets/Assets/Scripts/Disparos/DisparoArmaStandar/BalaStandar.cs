﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaStandar : MonoBehaviour
{
    public float tiempoVida = 5f;
    public int daño = 10;
    public GameObject ExplosionEffect;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tiempoVida -= Time.deltaTime;

        if(tiempoVida <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if(other.gameObject.GetComponent<VidaEnemigo>() != null)
        {
            VidaEnemigo enemigoCosas = other.gameObject.GetComponent<VidaEnemigo>();

            enemigoCosas.TakeDamage(daño);
            Instantiate(ExplosionEffect,transform.position,transform.rotation);

            Destroy(gameObject);
        }

        
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            Instantiate(ExplosionEffect, transform.position, transform.rotation);

            Destroy(gameObject);
        }

        if (collision.gameObject.GetComponent<VidaEnemigo>() != null)
        {
            VidaEnemigo enemigoVida = collision.gameObject.GetComponent<VidaEnemigo>();

            enemigoVida.TakeDamage(daño);
            Instantiate(ExplosionEffect, transform.position, transform.rotation);

            Destroy(gameObject);
        }

        if (collision.gameObject.GetComponent<VidaEnemigoPatrullero>() != null)
        {
            VidaEnemigoPatrullero enemigoVida = collision.gameObject.GetComponent<VidaEnemigoPatrullero>();

            enemigoVida.TakeDamage(daño);
            Instantiate(ExplosionEffect, transform.position, transform.rotation);

            Destroy(gameObject);
        }

    }
}
