﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoArmaExplosiva : MonoBehaviour
{
    public GameObject balaStandar;

    public float fuerzaBala = 5f;

    private float nexFire = 0.5f;
    private float myTime = 0.0f;
    public float velocidadDisparo = 0.1f;

    public Transform lugarDisparo;

    public MunicionesCantidad municion;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        myTime += Time.deltaTime;

        if (Input.GetButton("Fire1") && myTime > nexFire && municion.municionExplosiva > 0)
        {
            nexFire = myTime + velocidadDisparo;

            Shoot();

            nexFire = nexFire - myTime;
            myTime = 0.0f;
            municion.municionExplosiva--;
        }
    }

    void Shoot()
    {
        GameObject bullet = Instantiate(balaStandar, lugarDisparo.position, transform.rotation);
        Rigidbody rbBala = bullet.GetComponent<Rigidbody>();

        rbBala.AddForce(lugarDisparo.transform.forward * fuerzaBala, ForceMode.Impulse);
    }
}
