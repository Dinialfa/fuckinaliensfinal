﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ContadorBalas : MonoBehaviour
{
    public MunicionesCantidad municiones;

    public Text text;

    public WeaponManager weapon_Manager;
    void Start()
    {
        
        
    }

    
    void Update()
    {
        if (weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.DISPARONOMRAL)
        {
            text.text = municiones.devolverMunicionStandar().ToString();
        }

        if (weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.EXPLOSIVO)
        {
            text.text = municiones.devolverMunicionExplosiva().ToString();
        }
    }
}
